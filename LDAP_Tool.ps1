﻿# Variable list 
do{

Clear-Host

Write-Host "-----------------------------------------------------------------------------------------------------------------"
Write-Host "-----------------------------------------------------------------------------------------------------------------"
Write-Host "                                                                                                                 "
Write-host "                                      Welcome to the M159 LDAP Tool                                              "
Write-Host "                                                                                                                 "
Write-Host "-----------------------------------------------------------------------------------------------------------------"
Write-Host "-----------------------------------------------------------------------------------------------------------------"
Write-Host "                                                                                                                 "
Write-host "Please Choose one of the options. To select an option please enter the Number of the option below."
Write-Host "                                                                                                                 "
Write-Host "-----------------------------------------------------------------------------------------------------------------"
Write-Host "1. Create new User"
Write-Host "2. Delete User"
Write-Host "3. Show User from OU"
Write-Host "4. Show User from group"
Write-Host "5. Import User"
Write-Host "6. Exit"
Write-Host "-----------------------------------------------------------------------------------------------------------------"

$selected_option = Read-Host "Enter the Number of the Option you want to select"

if ($selected_option -eq 1){
    do{
    Clear-Host

    Write-Host "-----------------------------------------------------------------------------------------------------------------"
    Write-Host "-----------------------------------------------------------------------------------------------------------------"
    Write-Host "                                                                                                                 "
    Write-host "                                           Create new user                                                       "
    Write-Host "                                                                                                                 "
    Write-Host "-----------------------------------------------------------------------------------------------------------------"
    Write-Host "-----------------------------------------------------------------------------------------------------------------"

    $ou = Read-Host "Where do you want to create your User? (ou=Intern, Ou=Madetswil,ou=Workplace)"
    $Login = Read-Host "Loginname"
    $firstName = Read-Host "Firstname"
    $SurName = Read-Host "Surname" 
    [string]$Tele = Read-Host "Telephonenumber"

    $pw1=  Read-Host "Password" -AsSecureString
    $pw2 = Read-Host "Please confirm your Password" -AsSecureString
    $pw1_txt = [RunTime.InteropServices.Marshal]::PtrToStringAuto([Runtime.InteropServices.Marshal]::SecureStringToBSTR($pw1))
    $pw2_txt = [RunTime.InteropServices.Marshal]::PtrToStringAuto([Runtime.InteropServices.Marshal]::SecureStringToBSTR($pw2))

        while(($pw1_txt -cne $pw2_txt) -or ($pw1_txt -notmatch ".*\d+.*") -or ($pw1_txt -cnotmatch "[A-Z]") -or ($pw1_txt.Length -lt 8)){
            
            Write-Host "Invalid Password try again..."
            $pw1=  Read-Host "Password" -AsSecureString
            $pw2 = Read-Host "Please confirm your Password" -AsSecureString
            $pw1_txt = [RunTime.InteropServices.Marshal]::PtrToStringAuto([Runtime.InteropServices.Marshal]::SecureStringToBSTR($pw1))
            $pw2_txt = [RunTime.InteropServices.Marshal]::PtrToStringAuto([Runtime.InteropServices.Marshal]::SecureStringToBSTR($pw2))

                if ($pw1_txt -cne $pw2_txt){
                    Write-Host "Passwords do not match, try again."
                }

                elseif (($pw1_txt -notmatch ".*\d+.*")-or ($pw1_txt -cnotmatch "[A-Z]") -or ($pw1_txt.Length -lt 8)){
                    Write-Host "The password needs to contain 12 characters, with upper and lower case letters an numbers."
                }        
        }
    
    Write-Host "Checking the Information......"

        if($ou -eq " "){
            do{
                Write-Host "The Ou you selected doesn't exist"
                $ou = Read-Host "Where do you want to create your User? (ou=Intern, Ou=Madetswil,ou=Workplace)"
            }until($ou -ne " ")
        }
    
        if (($firstName -match ".*\d+.*") -or ($SurName -match ".*\d+.*") -or ($firstName -eq " ") -or ($SurName -eq " ")) {
            do{ 
                Write-Host "Your Name doesn't comply with the guidelines, please enter it again" -ForegroundColor Red
                $firstName = Read-Host "Firstname"
                $SurName = Read-Host "Surname" 
            }until(($firstName -notmatch ".*\d+.*") -and ($SurName -notmatch ".*\d+.*"))
        }

        if ($Tele -match "[A-Z]", " "){
            do{
                Write-Host "Telephonenumber is invalid" -ForegroundColor Red
                $Tele = Read-Host "Telephonenumber"
    
            }until($Tele -notmatch "[A-Z]")
        }

        Write-Host "You good to go, the User you requested will be created" -ForegroundColor Green
            try {
                Write-Host "-----------------------------------------------------------------------------------------------------------------"
                Write-Host "                                    Creating new User...Please Wait                                              " -ForegroundColor Green
                Write-Host "-----------------------------------------------------------------------------------------------------------------"
            
                #Enter-PSSession -ComputerName "winsrv01"
                #New-ADUser -Surname $SurName -GivenName $firstName -AccountPassword $pw -Path $ou -DisplayName $Login -OfficePhone $Tele
            
                Write-Host "-----------------------------------------------------------------------------------------------------------------"
                Write-Host "                                        New user created                                                         " -ForegroundColor Green
                Write-Host "-----------------------------------------------------------------------------------------------------------------"
            
                $c = Read-Host "Do you want to create an other user? y/n"
            }   
            catch{
        
                Write-Host "Something went wrong while creating the new user" -ForegroundColor Red
                $c = Read-Host "do you wanna try again?"
            }
                
}until ($c -eq "n")  
$repeat = Read-Host "Do you want to do something else ? y/n"         
}

if ($selected_option -eq "2"){
    do{
    Clear-Host

    Write-Host "-----------------------------------------------------------------------------------------------------------------"
    Write-Host "-----------------------------------------------------------------------------------------------------------------"
    Write-Host "                                                                                                                 "
    Write-host "                                             Delete user                                                         "
    Write-Host "                                                                                                                 "
    Write-Host "-----------------------------------------------------------------------------------------------------------------"
    Write-Host "-----------------------------------------------------------------------------------------------------------------"
    
    $del_usr = Read-Host "User to delete"

    $checkusr = get-aduser -identity $del_usr

    try {
    
        $checkusr = get-aduser -identity $del_usr

    }
    catch{

    Write-Host "The user you selected doesn't exist in the AD"
    $x = Read-Host "Try again y/n"
    
    
    }


}until($x -eq "n")

if ($selected_option -eq "3"){
    
    Clear-Host

    Write-Host "-----------------------------------------------------------------------------------------------------------------"
    Write-Host "-----------------------------------------------------------------------------------------------------------------"
    Write-Host "                                                                                                                 "
    Write-host "                                          Show User from OU                                                      "
    Write-Host "                                                                                                                 "
    Write-Host "-----------------------------------------------------------------------------------------------------------------"
    Write-Host "-----------------------------------------------------------------------------------------------------------------"
}

if ($selected_option -eq "4"){
    
    Clear-Host

    Write-Host "-----------------------------------------------------------------------------------------------------------------"
    Write-Host "-----------------------------------------------------------------------------------------------------------------"
    Write-Host "                                                                                                                 "
    Write-host "                                       Show User from group                                                      "
    Write-Host "                                                                                                                 "
    Write-Host "-----------------------------------------------------------------------------------------------------------------"
    Write-Host "-----------------------------------------------------------------------------------------------------------------"
}

if ($selected_option -eq "5"){
    
    Clear-Host

    Write-Host "-----------------------------------------------------------------------------------------------------------------"
    Write-Host "-----------------------------------------------------------------------------------------------------------------"
    Write-Host "                                                                                                                 "
    Write-host "                                           Import User                                                           "
    Write-Host "                                                                                                                 "
    Write-Host "-----------------------------------------------------------------------------------------------------------------"
    Write-Host "-----------------------------------------------------------------------------------------------------------------"
}

if ($selected_option -eq "6"){
    exit $code
}
}until($repeat -eq "n")